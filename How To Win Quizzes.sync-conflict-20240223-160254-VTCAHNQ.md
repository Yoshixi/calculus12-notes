---
aliases: 
tags:
  - meta
---
- [ ] Feb 13 Quiz
# General
- Rationalize when you can. Remember to multiply numerator **AND** **denominator** when you rationalize
# Unit 1 - Limits
- Abuse [[Limit Rationalization]]
- Abuse [[Limit Substitution]]
- Don't sub in to find limits. Use [[Limit Properties]]
	- Discontinuities can only be found with limits
- All lines should be in [[Standard Form]]
# Unit 2 - Derivatives
- Do not apply derivative power rule if you have a composite function. ![[How To Win Quizzes-20240215145945274.webp]]
- 'Determine the Slope' is not the same question as 'Determine the equation of the slope'. Former requires you to just get the slope value, latter is [[Tangent Equation At A Point]]