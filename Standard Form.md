---
tags:
  - advfunctions
---
- Equation includes x and y
- Equation equal to 0
- No fractions
- X coefficient is always positive
![[Standard Form-20240212133010187.webp]]
